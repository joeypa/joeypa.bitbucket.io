var pictureArray = ['dog','mouse','cat','frog','bug','fly','spider','bat','monkey'];
var pictureToGuess = randomPicture();

window.addEventListener("load", init);

function init(){
    showPictures();
    showPictureToGuess();
}

function showPictures(){

    //<img src='cat.jpg' id='cat' class='img--small'/>

        for (var index in pictureArray){
            var img = document.createElement('img');
            img.setAttribute('src','./images/'+pictureArray[index]+'.jpg');
            img.setAttribute('class', 'img--small');
            img.setAttribute('id', pictureArray[index]);
            img.addEventListener('click', handleImgClickEvent)
            var ph = document.getElementById('img-grid');
            ph.appendChild(img);
        }
    }
    function handleImgClickEvent(event){
        console.log(event.target.id);
        var chosenPicture = event.target.id;
        if (chosenPicture == pictureToGuess){
            console.log('Well done!');
            document.getElementById('message').innerHTML = 'Well done!';
            pictureToGuess = randomPicture();
            showPictureToGuess();
        }else{
            console.log('Try again!');
            document.getElementById('message').innerHTML = 'Try again!';
        }

    }
    function showPictureToGuess(){
        var img = document.getElementById('random-img');
        img.setAttribute('src', './images/'+pictureToGuess+'.jpg')
    }

    function randomPicture(){
        console.log(Math.floor(pictureArray.length * Math.random()));
        return pictureArray[Math.floor(pictureArray.length * Math.random())]
    }
